//scrollreveal

window.sr = ScrollReveal()
sr.reveal(
  '.navbar-brand', {
    duration: 2000,
    viewFactor: 0.2,
    reset: true
})

sr.reveal('.slidecaption1', {
  duration: 3000,
  origin: 'right',
  distance: '200px',
  delay: 200
})

$('#heroslider').on('slide.bs.carousel',function(){

  sr.reveal('.slidecaption1', {
    duration: 3000,
    origin: 'right',
    distance: '200px',
    delay: 200,
    reset: true
  })

  sr.reveal('.slidecaption2', {
    duration: 3000,
    origin: 'right',
    distance: '200px',
    delay: 200,
    reset: true
  })

  sr.reveal('.slidecaption3', {
    duration: 3000,
    origin: 'right',
    distance: '200px',
    delay: 200,
    reset: true
  })

  sr.reveal('.slidecaption4', {
    duration: 3000,
    origin: 'right',
    distance: '200px',
    delay: 200,
    reset: true
  })

})

// travel page

sr.reveal('.traveltitle', {
  duration: 3000,
  origin: 'right',
  distance: '200px',
  delay: 200,
  reset: true
})




// sr.reveal('.st1', {
//   duration: 2000,
//   origin: 'left',
//   distance: '300px',
//   delay: 1500
// })
//
// sr.reveal('.st2', {
//   duration: 2000,
//   origin: 'left',
//   distance: '300px',
//   delay: 3500
// })

sr.reveal('.lpinfo', {
  duration: 2000,
  origin: 'right',
  distance: '50px',
  delay: 200,
  viewFactor: 0.2
})

sr.reveal('.sc1', {
  duration: 1000,
  origin: 'top',
  distance: '60px',
  delay: 200,
  viewFactor: 0.2
})

sr.reveal('.sc2', {
  duration: 1000,
  origin: 'top',
  distance: '60px',
  delay: 1000
})

sr.reveal('.sc3', {
  duration: 1000,
  origin: 'top',
  distance: '60px',
  delay: 2000
})

// sr.reveal('.st3', {
//   duration: 200,
//   origin: 'left',
//   distance: '300px',
//   delay: 2400
// })
//
// sr.reveal('.st4', {
//   duration: 200,
//   origin: 'left',
//   distance: '300px',
//   delay: 2600
// })
//
// sr.reveal('.st5', {
//   duration: 200,
//   origin: 'left',
//   distance: '300px',
//   delay: 2800
// })
//
//
// sr.reveal('.subtitle',{
//   duration: 1000,
//   origin: 'left',
//   distance: '300px',
//   delay: 4800
// })
