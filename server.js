const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const nodemailer = require('nodemailer')
const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const config = require('./config/database')

require('dotenv').load()
//passport config

mongoose.connect(config.database)
let db = mongoose.connection

db.once('open', function(){
  console.log('Successfully connected to mongodb')
})


app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())


app.get('/', function(req, res){
  res.render('index')
})


app.listen('5000', function(req, res){
  console.log('SERVER RUNNING ON PORT 5000')
})
