const gulp = require('gulp')
const cleanCSS = require('gulp-clean-css')
const autoprefixer = require('gulp-autoprefixer')
const imagemin = require('gulp-imagemin')
const sass = require('gulp-sass')

gulp.task('gulpsass', () =>
  gulp.src('./public/sass/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./public/dist'))
)

gulp.task('prefixclean', () =>

    gulp.src('./public/dist/style.css')
     .pipe(autoprefixer({
       browsers: ['last 2 versions'],
       cascade: false
     }))
     .pipe(cleanCSS({compatibility: 'ie8'}))
     .pipe(gulp.dest('./public/dist'))
)

gulp.task('gulpstyle',  ['gulpsass', 'prefixclean'])

// gulp.task('minifycss', () =>
//   gulp.src('./public/dist/style.css')
//   .pipe(cleanCSS({compatibility: 'ie8'}))
//   .pipe(gulp.dest('./public/dist'))
// )

gulp.task('gulpimage', () =>
  gulp.src('./public/images/*')
   .pipe(imagemin())
   .pipe(gulp.dest('./public/imagesmin'))
)

gulp.task('watch', () =>
  gulp.watch('./public/sass/style.scss', ['gulpsass'])
)

gulp.task('build', ['gulpstyle', 'gulpimage'])

gulp.task('default', ['build'])
